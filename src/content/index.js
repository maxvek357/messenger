import React from 'react';

import {connect} from 'react-redux';

import Card from './cards/Card';
import Loader from '../components/Loader';
import Modal from '../portals/Modal'
import {cardActions} from '../actions';
import { history } from '../helpers';

import './cards/css/cardStyle.css';

class Content extends React.PureComponent {

  state = {
    isModalOpen: false,
    singlePost: false,
    extendComment: false,
  }

  componentDidMount(){
    const { user, postId, comId } = this.props.match.params;
    const { dispatch } = this.props;
    if (user && comId) {
      dispatch(cardActions.getComments(comId));
      this.setState({
        singlePost: true,
        extendComment: true
      }, null)
    } else if(user && postId){
      dispatch(cardActions.getCardsById(postId));
      this.setState({
        singlePost: true,
        extendComment: false
      }, null);
    } else if (user){
      dispatch(cardActions.getUserCards(user));
      this.setState({
        singlePost: false,
        extendComment: false
      }, null);
    } else {
      dispatch(cardActions.getAllCards());
      this.setState({
        singlePost: false,
        extendComment: false
      }, null);
    }
  }

  componentDidUpdate(){
    const { card: { cards, newCards } } = this.props;
    if ((cards && newCards) && (cards.length !== newCards.length)) {
      this.hasNewCard = true;
    } else {
      this.hasNewCard = false;
    }
  }

  componentWillUnmount(){
    this.hasNewCard = false;
  }

  addTwit = text => {
    const newCard = {
      id: null,
      time: Date.now(),
      picture: "",
      text: text,
      like: 0,
      retwit: 0,
      comments: 0,
      userId: this.props.user.id,
      userName: this.props.user.username
    }
    this.props.dispatch(cardActions.addCard(newCard));
    this.setState({isModalOpen: !this.state.isModalOpen});
  }

  onCardHandle = e => {
    const targ = e.target;
    switch (targ.className) {
      case "row align-items-start twit-card":
      case "twit-card-text":
        history.push("/" + targ.dataset.userName + "/" + targ.dataset.postId);
        break;
      case "userIco":
        history.push("/" + targ.dataset.userName);
        break;
      default:
        return false
    }
  }

  newCardHandle = () => {
    this.props.dispatch({type: "LOAD_NEW_DATA"});
    this.hasNewCard = false;
  }

  toggleModal = () => {
    this.setState({isModalOpen: !this.state.isModalOpen})
  }

  render(){
    const { card, loggedIn, isLoading } = this.props;
    const { singlePost, extendComment, isModalOpen } = this.state;
    const params = this.props.match.params;

    console.log("CONTENT", this.props);

    if (isLoading || Object.entries(card).length === 0){
      return(
        <div className="col-8">
          <div className="d-flex justify-content-center white-bg loading-content-block">
            <Loader/>
          </div>
       </div>
      )
    }
    return (
      <main className="col-8" onClick={this.onCardHandle}>
        {(loggedIn && Object.entries(params).length === 0) &&
          <div className="row justify-content-center white-bg new-post-block"
          onClick={this.toggleModal}>New Twit</div>}

        {isModalOpen &&
           <Modal
            onClick={this.toggleModal}
            porttype="newPost" modalRoot="modal-window"
            onSubmit={text => this.addTwit(text)}
            btnText="New Twit" />}

        {(this.hasNewCard && Object.entries(params).length === 0) &&
          <div className="row justify-content-center white-bg new-card-block"
          onClick={this.newCardHandle}>Load new twits!</div>}

        {Object.entries(card.cards).length !== 0 &&
          card.cards.map(card =>
            <Card key={card.id} card={card} singlePost={singlePost} extendComment={extendComment} />
          )}

      </main>
    )
  }
}

export default connect(
  state => ({
    card: state.card,
    isLoading: state.card.isLoading,
    loggedIn: state.authentication.loggedIn,
    user: state.authentication.user
  })
)(Content);
