import React from 'react';
import { Link } from 'react-router-dom'

import Avatar from '../../components/Avatar';
import ActionBoard from '../../components/ActionBoard';

import './css/index.css';

const Comment = ({ comment: { id, userComName, replaysName, comment, userId, postId }, extendComment }) => {

  if (typeof id === "undefined") {
    return(
      <>
      </>
    )
  }

  return(
    <>
      <div className={"white-bg new-post-block col-12" + (extendComment ? " replay-comment" : "")} key = {id}
           data-user-name = {userComName} data-com-id = {id} >
        <div className="d-flex flex-row">
          <div className="col-auto">
            <Link to={`/${userComName}`} >
              <Avatar avatarSize="50" />
            </Link>
          </div>
          <div className="col-auto">
            <div>
              <Link to={`/${userComName}`} >{userComName}</Link>
            </div>
            <div>
              {`Replying to `}
              {replaysName.map(name =>
                <Link to={`/${name}`} key = {name}> @{name} </Link>
              )}
            </div>
            <div>
              {comment}
            </div>
          <ActionBoard cid = {id} boardFor = "comment" cuserId = {userId} forPost = {postId} />
          </div>
        </div>
      </div>
    </>
  )
}

export default Comment
