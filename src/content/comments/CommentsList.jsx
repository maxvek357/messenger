import React from 'react';

import {connect} from 'react-redux';

import Comment from './Comment';
import Loader from '../../components/Loader';
import { history } from '../../helpers';

const CommentsList = ({ comment:{ comments, newComments, isLoading }, extendComment, dispatch, user, comcount }) => {

  const handleClick = e => {
    const targ = e.target;
    if (targ.className === "white-bg new-post-block col-12" ||
        targ.className === "white-bg new-post-block col-12 replay-comment") {
      history.push("/" + targ.dataset.userName + "/status/" + targ.dataset.comId);
    } else if (targ.className === "d-flex flex-row") {
      const {userName, comId} = targ.parentNode.dataset;
      history.push("/" + userName + "/status/" + comId);
    }
  }

  if (isLoading) {
    return(
      <div className="row white-bg new-post-block">
        <div className="col-12 d-flex justify-content-center">
          <Loader />
        </div>
      </div>
    )
  }

  if (comcount === 0 || comments === undefined || Object.entries(comments).length === 0) {
    return  <div className="row card-bottom" />
  }

  return(
    <>
      <div className="row comment-list-row" onClick = {handleClick} >
        {comments.map(item =>
          <Comment comment={item} key={item.id} extendComment={extendComment} />
        )}
      </div>
      <div className="row card-bottom" />
    </>
  )
}

export default connect(
  state => ({
    comment: state.comment
  })
) (CommentsList)
