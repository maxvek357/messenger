import React from 'react';
import { Link } from 'react-router-dom';

import Avatar from '../../components/Avatar';
import CommentsList from '../comments/CommentsList';
import ActionBoard from '../../components/ActionBoard';
import PostTime from '../../components/PostTime';

import './css/cardStyle.css';
import userPostImg from '../../img/post.jpeg';

const Card = ({card : { id, time, text, userId, comments, userName }, singlePost, extendComment}) =>  {

  return(
    <>
      <div className="row align-items-start twit-card" data-post-id={id} data-user-name={userName}>
        <div className="col-auto">
          <Avatar avatarSize="75" uid={id} uname={userName} />
        </div>
        <div className="col">
          <ul className="list-inline">
            <li className="list-inline-item">
              <Link to={`/${userName}`}>{userName}</Link>
            </li>
            <li className="list-inline-item">
              <PostTime time={time} />
            </li>
          </ul>
          <p className="twit-card-text" data-post-id={id} data-user-name={userName}>
            {text}
          </p>
          <div className="twit-card-image">
            <Link to={`/${userName}/${id}`}>
              <img className="" src={userPostImg} alt="twit" />
            </Link>
          </div>
          <ActionBoard cid={id} boardFor="card" cuserId={userId} comcount={comments} />
        </div>
      </div>

      {singlePost && <CommentsList extendComment={extendComment} comcount={comments} />}

    </>
  )
}

export default Card
