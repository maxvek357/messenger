import React from 'react';

import Input from '../components/Input';
import Button from '../components/Button';

class Logforportal extends React.Component {

  state = {
    btnText: "Log in",
    uName: '',
    uPass: ''
  }

  handleClick = () => {
    if(this.state.uName !== ''){
      this.props.onClick(this.state.uName, this.state.uPass)
      this.setState({
        uName: "",
        uPass: ""
      })
    }
  }

  onChange = e => {
    this.setState({
      [e.target.name] : e.target.value
    }, () => console.log(this.state))
  }

  render() {
    return(
      <>
      <Input
        labelText="Log in" name="uName"
        onChange={this.onChange} value={this.state.uName} />
      <Input
        labelText="Password" name="uPass"
        onChange={this.onChange} value={this.state.uPass} />
      <Button btnText={this.state.btnText} onClick={this.handleClick} />
      </>
    )
  }
}

export default Logforportal
