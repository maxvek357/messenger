import React from 'react';
import ReactDOM from 'react-dom'

import withModal from '../hoc/modalHOC'

import './css/portal.css'

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.el = document.createElement('div');
    this.modalRoot = document.getElementById(this.props.modalRoot);
  }

  componentDidMount() {
    const { modalRoot, errorMessage } = this.props;
    if (modalRoot === "header") {
      this.el.classList.add("login-portal")
    } else if (errorMessage && modalRoot === "modal-window") {
      this.el.classList.add("error-portal")
    } else if (modalRoot === "modal-window") {
      this.el.classList.add("modal-block")
    }
    this.modalRoot.appendChild(this.el);
  }

  componentWillUnmount() {
    this.modalRoot.removeChild(this.el);
  }

  render(){
    return ReactDOM.createPortal(
      <>
        {this.props.children}
      </>,
      this.el,
    );
  }
}
export default withModal(Modal)
