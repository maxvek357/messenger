let likes = JSON.parse(localStorage.getItem('likes'));
let retwits = JSON.parse(localStorage.getItem('retwits'));

export const handleAction = ({targId, action, uid, boardFor}) => {
  const cell = (boardFor === "card") ? "postId" : "commentId";
  let arr = (action === "like" || action === "unlike") ? likes : retwits
  if (action === "like" || action === "retwit") {
    const matchArr = arr.filter(elem => elem[`${cell}`] === targId);
    if (matchArr.length) {
      let userSet = new Set(matchArr[0].userId)
      userSet.add(uid)
      arr.find(elem => elem[`${cell}`] === targId).userId = [...userSet];
    } else {
      let newItem = {}
      if (boardFor === "card") {
        newItem = {
         postId: targId,
         commentId: null,
         userId: [uid]
       }
      } else if (boardFor === "comment") {
        newItem = {
         postId: null,
         commentId: targId,
         userId: [uid]
       }
      }
      arr.push(newItem);
    }
  } else {
    let filteredId = new Set(arr.find(elem => elem[`${cell}`] === targId).userId);
    filteredId.delete(uid);

    arr.find(elem => elem[`${cell}`] === targId).userId = [...filteredId];
  }

  return arr;
}
