import {handleResponse} from './handleResponse.service'

const apiUrl = "http://localhost:3000"

export const cardService = {
    addCard,
    getAllCards,
    getUserCards,
    getCardsById,
    getBoardInfo,
    userCardActions,
    addComment,
    getComments
};

function addCard(card) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(card)
    };
    return fetch(`${apiUrl}/cards/newcard`, requestOptions).then(handleResponse);
}

function getAllCards() {
    const requestOptions = {
        method: 'GET'
    };
    return fetch(`${apiUrl}/cards`, requestOptions).then(handleResponse);
}

function getUserCards(uName) {
    const requestOptions = {
        method: 'GET'
    };
    return fetch(`${apiUrl}/cards/${uName}`, requestOptions).then(handleResponse);
}

function getCardsById(id) {
    const requestOptions = {
        method: 'GET'
    };
    return fetch(`${apiUrl}/cards/${id}`, requestOptions).then(handleResponse);
}

function getBoardInfo() {
    const requestOptions = {
        method: 'GET'
    };
    return fetch(`${apiUrl}/boards`, requestOptions).then(handleResponse);
}

function userCardActions(actionObj) {
  const user = JSON.parse(localStorage.getItem('user'));
  const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json', 'Authorization': user.token },
      body: JSON.stringify(actionObj)
  };

  return fetch(`${apiUrl}/userCardActions`, requestOptions).then(handleResponse);
}

function addComment(comment) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(comment)
    };
    return fetch(`${apiUrl}/comment/new`, requestOptions).then(handleResponse);
}

function getComments(id) {
    const requestOptions = {
        method: 'GET'
    };
    return fetch(`${apiUrl}/comments/${id}`, requestOptions).then(handleResponse);
}
