import React from 'react';

import Logforportal from '../portals/Logforportal';
import Button from '../components/Button';
import Form from '../components/Form';

import closeSVG from '../img/svg/close.svg';

const withModal = WrappedComponent => {
  return class extends React.Component {
    state = {
      btnText: "Log out"
    }
    render() {
      if (this.props.porttype === "login"){
        return (
          <WrappedComponent {...this.props}>
            <Logforportal onClick={this.props.onClick} />
          </WrappedComponent>
        )
      } else if (this.props.porttype === "logout") {
        return (
          <WrappedComponent {...this.props}>
            <Button onClick={this.props.onClick} btnText={this.state.btnText}/>
          </WrappedComponent>
        )
      } else if (this.props.porttype === "newPost") {
        return (
          <WrappedComponent {...this.props}>
            <div className="white-bg modal-block-content">
              <img src={closeSVG} className="modal-close" onClick={this.props.onClick} alt="close"/>
              <Form onSubmit={this.props.onSubmit} btnText={this.props.btnText}/>
            </div>
          </WrappedComponent>
        )
      } else if (this.props.porttype === "alert") {
        return (
          <WrappedComponent {...this.props}>
            <div className={this.props.errortype}>
              {this.props.errorMessage}
            </div>
          </WrappedComponent>
        )
      }

      return (
        <WrappedComponent {...this.props}></WrappedComponent>
      );
    }
  };
}

export default withModal
