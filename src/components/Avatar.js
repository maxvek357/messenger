import React from 'react';

import userAvatarImg from '../img/avatar.png'

const Avatar = ({avatarSize, uid = "", uname = ""}) =>  {

  const size = {
    width: Number(avatarSize),
    height: Number(avatarSize)
  }

  return(
    <>
      <img src={userAvatarImg}
          alt="user icon"
          style={size}
          className="userIco"
          data-user-name={uname} />
    </>
  )
}

export default Avatar
