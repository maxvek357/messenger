import React from 'react';

const PostTime = ({time}) => {

  const [postTime, setPostTime] = React.useState(time);



  React.useEffect(() => {
    newTime()
  },[]);

  React.useEffect(() => {
    let timerID = setInterval(
      () => newTime(),
      30000
    );
    return () => {
      clearInterval(timerID);
    }
  });

  const newTime = () => {
    const curdate = parseInt((Date.now() - (time - 1)) / 1000);
    if (curdate < 59) {
      setPostTime(parseInt(curdate) + "sec");
    } else if (curdate > 60 && curdate < 3599) {
      setPostTime(parseInt(curdate / 60) + "min");
    } else if (curdate > 3600 && curdate < 86399) {
      setPostTime(parseInt(curdate / 3600) + "hours");
    } else {
      setPostTime(parseInt(curdate / 86400) + "days");
    }
  }

  return(
    <>{postTime}</>
  )
}

export default PostTime
