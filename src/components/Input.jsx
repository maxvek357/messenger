import React from 'react'

const Input = ({labelText, name, onChange, value}) => {
  return(
    <div className="form-group">
      <label>{labelText}</label>
      <input
        type="text"
        name={name}
        className="form-control form-control-sm"
        value={value}
        onChange={onChange} />
    </div>
  )
}

export default Input
