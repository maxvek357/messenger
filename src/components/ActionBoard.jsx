import React, {useState} from 'react';

import {connect} from 'react-redux';

import {cardActions} from '../actions';
import Modal from '../portals/Modal';


import chatSVG from '../img/svg/chat.svg';
import heartSVG from '../img/svg/heart.svg';
import repeatSVG from '../img/svg/repeat.svg';

const ActionBoard = ({ cid, boardFor, cuserId, comcount = 0, forPost = null,
  dispatch, likes, retwits, comment: { comments, isLoading }, user }) => {

  const uid = user ? user.id : null;

  const [like, setLike] = useState(0);
  const [retwit, setRetwit] = useState(0);
  const [comment, setComment] = useState(comcount);
  const [isModalOpen, setModalState] = useState(false);

  React.useEffect(() => {
    switch (boardFor) {
      case "card":
        retwits.find(retwit => retwit.postId === cid ? setRetwit(retwit.userId.length) : null);
        likes.find(like => like.postId === cid ? setLike(like.userId.length) : null);
        if (!isLoading && !comcount && comments && Object.entries(comments).length !== 0) {
          setComment(comments.filter(com => com.postId === cid).length);
        }
        break;
      case "comment":
        retwits.find(retwit => retwit.commentId === cid ? setRetwit(retwit.userId.length) : null);
        likes.find(like => like.commentId === cid ? setLike(like.userId.length) : null);
        if (!isLoading && !comcount && comments && Object.entries(comments).length !== 0) {
          setComment(comments.find(com => com.id === cid).replays);
        }
        break;
      default:
        break;
    }
  });

  const onUserActionsHandle = e => {
    const targ = e.target.parentNode;
    if (uid) {
      switch (targ.className) {
        case "like-btn":
          handleTrigger(targ)
          dispatch(cardActions.like({
            targId: cid,
            action: targ.dataset.userAction,
            boardFor: boardFor,
            uid: uid
          }));
          break;
        case "retwit-btn":
          if (cuserId !== uid) {
            handleTrigger(targ);
            dispatch(cardActions.retwit({
              targId: cid,
              action: targ.dataset.userAction,
              boardFor: boardFor,
              uid: uid
            }));
          }
          break;
        case "comment-btn":
          toggleModal();
          break;
        default:
          return false
      }
    }
  }

  const handleTrigger = el => {
    const type = el.dataset.userAction;
    switch (type) {
      case "like":
        setLike(like - 1);
        el.dataset.userAction = "unlike";
        break;
      case "unlike":
        setLike(like + 1);
        el.dataset.userAction = "like";
        break;
      case "retwit":
        setRetwit(retwit - 1);
        el.dataset.userAction = "unretwit";
        break;
      case "unretwit":
        setRetwit(retwit + 1);
        el.dataset.userAction = "retwit";
        break;
      case "newComent":
        toggleModal();
        break;
      default:
        return null

    }
  }

  const toggleModal = () => {
    setModalState(!isModalOpen);
  }

  const addComment = text => {
    let newComment;
    if (boardFor === "comment") {
      newComment = {
        postId: forPost,
        comment: text,
        replaysTo: cid
      }
    } else {
      newComment = {
        postId: cid,
        comment: text
      }
    }
    dispatch(cardActions.addComment({comment: newComment, uid: uid, commentFor: boardFor}))
    setComment(comment + 1);
    toggleModal();
  }

  const setDataset = (id, arr, type) => {
    switch (type) {
      case "likes":
        if (arr) {
          if (arr.userId.find(userId => userId === id)) {
            return "like";
          }
          return "unlike";
        } else {
          return "unlike";
        }
      case "retwits":
        if (arr) {
          if (arr.userId.find(postsId => postsId === id)) {
            return "retwit";
          }
          return "unretwit";
        } else {
          return "unretwit";
        }
      default:
        return "";
    }
  }

  if (uid) {
    if (boardFor === "comment") {
      var uLike = likes.find(likes => likes.commentId === cid);
      var uRetwit = retwits.find(retwit => retwit.commentId === cid);
    } else {
      uLike = likes.find(likes => likes.postId === cid);
      uRetwit = retwits.find(retwit => retwit.postId === cid);
    }
  }

  return(
    <>
      <div className="row d-flex action-board" onClick={onUserActionsHandle} >
        <div className="like-btn" data-user-action={setDataset(uid, uLike, "likes")}>
          <img src={heartSVG} alt="like" />
          <span>{like}</span>
        </div>
        <div className="retwit-btn" data-user-action={setDataset(uid, uRetwit, "retwits")}>
          <img src={repeatSVG} alt="retwit" />
          <span>{retwit}</span>
        </div>
        <div className="comment-btn" data-user-action="newComent">
          <img src={chatSVG} alt="new comment" />
          <span>{comment}</span>
        </div>
      </div>

      {isModalOpen &&
         <Modal
          onClick={toggleModal}
          porttype="newPost" modalRoot="modal-window"
          onSubmit={text => addComment(text)}
          btnText="New Comment" />}
    </>
  )
}

export default connect(
  state => ({
    likes: state.like,
    retwits: state.retwit,
    comment: state.comment,
    user: state.authentication.user
  })
)(ActionBoard)
