import React from 'react'

const Button = ({btnText, onClick}) => {
  return(
    <button
      type="submit"
      className="btn btn-primary mb-2"
      onClick={onClick}>
        {btnText}
    </button>
  )
}

export default Button
