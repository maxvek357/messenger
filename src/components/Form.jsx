import React from 'react';

import Textarea from './Textarea';
import Button from './Button';

class Form extends React.Component {
  state = {
    text: ""
  }

  handleInputChange = e => {
    const target = e.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name] : value
    })
  }

  handleSubmit = e => {
    e.preventDefault();
    if(this.state.text !== ""){
      this.props.onSubmit(this.state.text);
      this.setState({
        text: ""
      })
    }
  }

  render(){
    return(
      <form>
        <Textarea
          name="text"
          value={this.state.text}
          onChange={this.handleInputChange} />
        <Button
          btnText={this.props.btnText}
          onClick={this.handleSubmit} />
      </form>
    )
  }
}

export default Form
