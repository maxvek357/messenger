import React from 'react'

const Textarea = ({name, onChange, value}) => {
  return(
    <div className="form-group">
      <label>Text</label>
      <textarea
        name={name}
        className="form-control"
        value={value}
        onChange={onChange} />
    </div>
  )
}

export default Textarea
