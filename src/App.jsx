import React from 'react';
import { Route } from "react-router-dom";

import { connect } from 'react-redux';

import Header from './header';
import Content from './content';
import Sidebar from './sidebar';
import Modal from './portals/Modal';
import { history } from './helpers';
import { alertActions, cardActions } from './actions';

import 'bootstrap/dist/css/bootstrap.css';

class App extends React.Component {
  constructor(props) {
    super(props);

    const { dispatch } = this.props;
    history.listen((location, action) => {
      dispatch(alertActions.clear());
    });
    setInterval(() => (
      dispatch(cardActions.getBoardInfo()),
      dispatch(cardActions.getCardTimeOut())
    ), 10000);
  }

  render() {
    const { _alert, dispatch } = this.props;

    if (Object.entries(_alert).length !== 0) {
      setTimeout(() => dispatch(alertActions.clear()), 5000)
    }

    return (
      <div className="wrapper">
        <Header />
        <section className="container">
          <div className="row pt-3">
            <Route path="/" component={Content} exact />
            <Route path="/:user" component={Content} exact />
            <Route path="/:user/:postId" component={Content} exact />
            <Route path="/:user/status/:comId" component={Content} exact />

            <Sidebar />
          </div>
        </section>
        {Object.entries(_alert).length !== 0 &&
          <Modal
           porttype="alert" modalRoot="modal-window"
           errorMessage={_alert.message}
           errortype={_alert.type} />
        }
      </div>
    );
  }
}

export default connect(
  state => ({
    _alert: state._alert,
  })
)(App);
