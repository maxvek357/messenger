import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { Router } from "react-router-dom";
import { CookiesProvider } from 'react-cookie';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import reducer from './reducer';
import { syncHistoryWithStore } from 'react-router-redux';
import logger from 'redux-logger'

import App from './App';

import { configureteBackend, history } from './helpers';

import './index.css';

configureteBackend()

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk, logger)));
const historyRed = syncHistoryWithStore(history, store);

ReactDOM.render(
  <CookiesProvider>
    <Provider store={store}>
      <Router history={historyRed}>
        <App />
      </Router>
    </Provider>
  </CookiesProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
