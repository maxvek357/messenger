import { userService } from '../services';
import { alertActions } from './';

export const userActions = {
    login,
    getAll,
    logout
};

function login(name, pass) {
  return dispatch => {
    dispatch(request({name}))

    userService.login(name, pass)
      .then(
        user => {
          dispatch(success(user));
          dispatch(alertActions.success(`Welcome ${user.username} !!!`))
        },
        error => {
          dispatch(failure());
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request(user)  { return { type: "LOGIN_REQUEST", user  } }
  function success(user)  { return { type: "LOGIN_SUCCESS", user  } }
  function failure() { return { type: "LOGIN_FAILURE" } }
}

function getAll() {
  return dispatch => {
    dispatch(request());

    userService.getAll()
      .then(
        users => dispatch(success(users)),
        error => {
          dispatch(failure());
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request() { return { type: "GETALL_REQUEST" } }
  function success(users) { return { type: "GETALL_SUCCESS", users } }
  function failure() { return { type: "GETALL_FAILURE" } }
}

function logout() {
  userService.logout();
  return { type: "LOGOUT" };
}
