import { cardService, handleAction } from '../services';
import { alertActions } from './';

export const cardActions = {
    addCard,
    getAllCards,
    getUserCards,
    getCardsById,
    getBoardInfo,
    getCardTimeOut,
    like,
    retwit,
    addComment,
    getComments
};



function addCard(card) {
  return dispatch =>{
    dispatch(request())

    cardService.addCard(card)
      .then(
        cards => dispatch(success(cards)),
        error => {
          dispatch(failure());
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request() { return { type: "ADD_CARD_REQUEST" } }
  function success(cards) { return { type: "ADD_CARD_SUCCESS", cards } }
  function failure() { return { type: "ADD_CARD_FAILURE" } }
}

function getAllCards() {
  return dispatch =>{
    dispatch(request())

    cardService.getAllCards()
      .then(
        item => {
          dispatch(successCard(item.card));
          dispatch(successComment(item.comment));
        },
        error => {
          dispatch(failure());
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request() { return { type: "GET_CARDS_REQUEST" } }
  function successCard(cards) { return { type: "GET_CARDS_SUCCESS", cards } }
  function successComment(comments) { return { type: "GET_COMMETS_SUCCESS", comments } }
  function failure() { return { type: "GET_CARDS_FAILURE" } }
}

function getUserCards(uName) {
  return dispatch =>{
    dispatch(request())

    cardService.getUserCards(uName)
      .then(
        cards => dispatch(success(cards)),
        error => {
          dispatch(failure());
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request() { return { type: "GET_CARDS_REQUEST" } }
  function success(cards) { return { type: "GET_CARDS_SUCCESS", cards } }
  function failure() { return { type: "GET_CARDS_FAILURE" } }
}

function getCardsById(id) {
  return dispatch =>{
    dispatch(request())

    cardService.getCardsById(id)
      .then(
        item => {
          const { card, comment } = item;
          dispatch(successCard(card));
          dispatch(successComment(comment));
        },
        error => {
          dispatch(failure());
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request() { return { type: "GET_CARDS_REQUEST" } }
  function successCard(cards) { return { type: "GET_CARDS_SUCCESS", cards } }
  function successComment(comments) { return { type: "GET_COMMETS_SUCCESS", comments } }
  function failure() { return { type: "GET_CARDS_FAILURE" } }
}

function getBoardInfo(){
  return dispatch =>{

    cardService.getBoardInfo()
      .then(
        data => {
          const { like, retwit } = data;
          dispatch(successLike(like));
          dispatch(successRetwit(retwit));
        },
        error => {
          dispatch(failure());
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function successLike(like) { return { type: "LIKE_GET", like } }
  function successRetwit(retwit) { return { type: "RETWIT_GET", retwit } }
  function failure(){ return { type: "GET_BOARD_INFO_FAILURE" } }
}

function getCardTimeOut(){
  return dispatch => {
    cardService.getAllCards()
      .then(
        data => {
          dispatch(successCard(data.card))
          dispatch(successComm(data.comment))
        },
        error => {
          dispatch(failure());
          dispatch(alertActions.error(error.toString()));
        }
      )
  }

  function successCard(data) { return { type: "GET_CARDS_SUCCESS_TO", data } }
  function successComm(data) { return { type: "GET_COMMENTS_SUCCESS_TO", data } }
  function failure() { return { type: "GET_CARDS_FAILURE_TO" } }
}

function like(like) {
  return dispatch =>{
    const likeresult = handleAction(like);
    dispatch(success(likeresult));

    cardService.userCardActions(like)
      .catch(
        error => {
          dispatch(failure());
          dispatch(alertActions.error(error.toString()));
        }
      );
  };
  function success(like) { return { type: "LIKE_SUCCESS", like } }
  function failure() { return { type: "LIKE_FAILURE" } }
}

function retwit(retwit) {
  return dispatch =>{
    const retwitresult = handleAction(retwit);
    dispatch(success(retwitresult));

    cardService.userCardActions(retwit)
      .catch(
        error => {
          dispatch(failure());
          dispatch(alertActions.error(error.toString()));
        }
      );
  };
  function success(retwits) { return { type: "RETWIT_SUCCESS", retwits } }
  function failure() { return { type: "RETWIT_FAILURE" } }
}

function addComment(comment) {
  return dispatch =>{
    dispatch(request())

    cardService.addComment(comment)
      .then(
        comments => dispatch(success(comments)),
        error => {
          dispatch(failure());
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request() { return { type: "ADD_COMMETS_REQUEST" } }
  function success(comments) { return { type: "ADD_COMMETS_SUCCESS", comments } }
  function failure() { return { type: "ADD_COMMETS_FAILURE" } }
}

function getComments(id) {
  return dispatch => {
    dispatch(request())

    cardService.getComments(id)
      .then(
        item => {
          dispatch(successCard(item.card));
          dispatch(successComment(item.comment));
        },
        error => {
          dispatch(failure());
          dispatch(alertActions.error(error.toString()));
        }
      );
  }
  function request() { return { type: "GET_COMMETS_REQUEST" } }
  function successCard(cards) { return { type: "GET_CARDS_SUCCESS", cards } }
  function successComment(comments) { return { type: "GET_COMMETS_SUCCESS", comments } }
  function failure() { return { type: "GET_COMMETS_FAILURE" } }
}
