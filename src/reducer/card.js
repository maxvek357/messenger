export default function twitlist(state = {}, action) {
  switch (action.type) {
    case "ADD_CARD_REQUEST":
    case "GET_CARDS_REQUEST":
      return { isLoading: true };
    case "ADD_CARD_SUCCESS":
    case "GET_CARDS_SUCCESS":
      return {
        cards: action.cards.reverse()
      }
    case "GET_CARDS_SUCCESS_TO":
      if (state.cards && (action.data.length !== state.cards.length)) {
        return {
          newCards: action.data.reverse(),
          cards: state.cards
        }
      } else {
        return state;
      }
    case "LOAD_NEW_DATA":
      return {
        cards: state.newCards
      }
    case "ADD_CARD_FAILURE":
    case "GET_CARDS_FAILURE":
    case "GET_CARDS_FAILURE_TO":
      return state;
    default:
      return state;
  }
}
