let retwits = JSON.parse(localStorage.getItem('retwits'));
const initialState = retwits ? retwits : [];

export default function like(state = initialState, action) {
  switch (action.type) {
    case "RETWIT_SUCCESS":
      return action.retwits;
    case "RETWIT_FAILURE":
      return state;
    case "RETWIT_GET":
      return action.retwit;
    default:
      return state
  }
}
