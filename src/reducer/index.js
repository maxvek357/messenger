import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux'

import authentication from './authentication';
import like from './like';
import retwit from './retwit';
import comment from './comment';
import card from './card';
import board from './board';
import _alert from './alert';

export default combineReducers({
  routing: routerReducer,
  authentication,
  like,
  retwit,
  comment,
  card,
  board,
  _alert
});
