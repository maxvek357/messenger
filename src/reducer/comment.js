export default function comment(state = {}, action) {
  switch (action.type) {
    case "GET_COMMETS_REQUEST":
    return {isLoading: true};
    case "ADD_COMMETS_REQUEST":
      return state;
    case "GET_COMMETS_SUCCESS":
    case "ADD_COMMETS_SUCCESS":
      return {
        comments: action.comments
      };
    case "GET_COMMENTS_SUCCESS_TO":
      if (state.comments && (action.data.length !== state.comments.length)) {
        return {
          newComments: action.data,
          comments: state.comments
        }
      } else {
        return state;
      }
    case "LOAD_NEW_DATA":
      return {
        comments: state.newComments
      }
    case "GET_COMMETS_FAILURE":
    case "ADD_COMMETS_FAILURE":
      return state;
    default:
      return state;
  }
}
