let likes = JSON.parse(localStorage.getItem('likes'));
const initialState = likes ? likes : [];

export default function like(state = initialState, action) {
  switch (action.type) {
    case "LIKE_SUCCESS":
      return action.like;
    case "LIKE_FAILURE":
      return state;
    case "LIKE_GET":
      return action.like;
    default:
      return state
  }
}
