export default function board(state = [], action) {
  switch (action.type) {
    case "GET_BOARD_INFO_SUCCESS":
      return action.data;
    case "GET_BOARD_INFO_FAILURE":
      return action.error;
    default:
      return state
  }
}
