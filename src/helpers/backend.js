import {users, cards, likes, retwits, comments} from '../storage'

localStorage.setItem('cards', JSON.stringify(cards));
localStorage.setItem('likes', JSON.stringify(likes));
localStorage.setItem('retwits', JSON.stringify(retwits));
localStorage.setItem('comments', JSON.stringify(comments));

const plusAction = ({targId, boardFor}, uid, arr, actType) => {
  const cell = (boardFor === "card") ? "postId" : "commentId";
  const matchArr = arr.filter(elem => elem[`${cell}`] === targId);
  if (matchArr.length) {
    let userSet = new Set(matchArr[0].userId)
    userSet.add(uid)
    arr.find(elem => elem[`${cell}`] === targId).userId = [...userSet];
  } else {
    let newItem = {}
    if (boardFor === "card") {
      newItem = {
       postId: targId,
       commentId: null,
       userId: [uid]
     }
    } else if (boardFor === "comment") {
      newItem = {
       postId: null,
       commentId: targId,
       userId: [uid]
     }
    }
    arr.push(newItem);
  }

  localStorage.setItem(`${actType}s`, JSON.stringify(arr));
}

const minusAction = ({targId, boardFor}, uid, arr, actType) => {
  const cell = boardFor === "card" ? "postId" : "commentId";

  let filteredId = new Set(arr.find(elem => elem[`${cell}`] === targId).userId);
  filteredId.delete(uid);

  arr.find(elem => elem[`${cell}`] === targId).userId = [...filteredId];

  localStorage.setItem(`${actType}s`, JSON.stringify(arr));
}

export function configureteBackend() {
  let realFetch = window.fetch;
  window.fetch = function (url, opts) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {

        // authenticate
        if (url.endsWith('/users/authenticate') && opts.method === 'POST') {
            // get parameters from post request
            let params = JSON.parse(opts.body);

            // find if any user matches login credentials
            let filteredUsers = users.filter(user => {
                return user.username.toLowerCase() === params.username.toLowerCase()
                && user.password === params.password;
            });

            if (filteredUsers.length) {
                // if login details are valid return user details and fake jwt token
                let user = filteredUsers[0];
                let responseJson = {
                  id: user.id,
                  firstName: user.firstName,
                  username: user.username,
                  token: 'fake-jwt-token'
                };
                resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(responseJson)) });
            } else {
                // else return error
                reject('Username or password is incorrect');
            }

            return;
        }

        // get users
        if (url.endsWith('/users') && opts.method === 'GET') {
            resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(users))});

            return;
        }

        // get user by id
        if (url.match(/\/users\/\d+$/) && opts.method === 'GET') {
            let urlParts = url.split('/');
            let id = parseInt(urlParts[urlParts.length - 1]);
            let matchedUsers = users.filter(user => { return user.id === id; });
            let user = matchedUsers.length ? matchedUsers[0] : null;

            resolve({ ok: true, text: () => JSON.stringify(user)});

            return;
        }

        // register user
        if (url.endsWith('/users/register') && opts.method === 'POST') {
            // get new user object from post body
            let newUser = JSON.parse(opts.body);

            // validation
            let duplicateUser = users.filter(user => { return user.username === newUser.username; }).length;
            if (duplicateUser) {
                reject('Username "' + newUser.username + '" is already taken');
                return;
            }

            // save new user
            newUser.id = users.length ? Math.max(...users.map(user => user.id)) + 1 : 1;
            users.push(newUser);
            localStorage.setItem('users', JSON.stringify(users));

            // respond 200 OK
            resolve({ ok: true, text: () => Promise.resolve() });

            return;
        }

        //Add new Card
        if (url.endsWith('/cards/newcard') && opts.method === 'POST') {
          let newPost = JSON.parse(opts.body);

          newPost.id = cards.length ? Math.max(...cards.map(card => card.id)) + 1 : 1;
          cards.push(newPost);
          localStorage.setItem('cards', JSON.stringify(cards));

          resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(cards)) });

          return;
        }

        //GET all Cards
        if (url.endsWith('/cards') && opts.method === 'GET') {
          let commentArr = [];

          cards.forEach((elem) => {
            comments.forEach((item) => {
              if(elem.id === item.postId){
                commentArr.push(item)
              }
            })
          })

          resolve({ ok: true, text: () => Promise.resolve(JSON.stringify({
            card: cards,
            comment: commentArr
          }))});

          return;
        }

        //GET Card by id
        if (url.match(/\/cards\/\d+$/) && opts.method === 'GET') {
          let urlParts = url.split('/');
          let id = parseInt(urlParts[urlParts.length - 1]);
          let matchedCards = cards.filter(card => card.id === id);
          if (matchedCards.length) {
            let card = matchedCards;
            let commentArr = [];

            cards.forEach((elem) => {
              comments.forEach((item) => {
                if(elem.id === item.postId){
                  commentArr.push(item)
                }
              })
            })

            resolve({ ok: true, text: () => Promise.resolve(JSON.stringify({card: card, comment: commentArr}))});

          } else {
            reject('There is no post witch this id');
          }

          return;
        }

        //GET Cards by userName
        if (url.match(/\/cards\/\w+$/) && opts.method === 'GET') {
          let urlParts = url.split('/');
          let uName = urlParts[urlParts.length - 1];
          const user = users.find(user => user.username === uName);

          if (user) {
            let matchedCards = cards.filter( card => card.userName === uName ? true : false).map(card => card);
            let card = matchedCards.length ? matchedCards : {};

            let uretw = [];

            retwits.filter(retwit => retwit.userId.find(
              item => item === user.id
            ) ? uretw.push(retwit.postId) : null)

            if (uretw) {
              for(let i = 0; i < uretw.length; i++){
                card.push(cards.filter(item => item.id === uretw[i])[0]);
              }
            }

            resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(card))});
          } else {
            reject('No user match your query');
          }

          return;
        }

        //Card actions( like, retwit)
        if (url.endsWith('/userCardActions') && opts.method === 'POST') {
          if (opts.headers && opts.headers.Authorization === 'fake-jwt-token') {
            const actionObj = JSON.parse(opts.body);
            const { action, uid } = actionObj;
            if (action === "like") {

              plusAction(actionObj, uid, likes, "like");

            } else if (action === "unlike") {

              minusAction(actionObj, uid, likes, "like");

            } else if (action === "retwit") {

              plusAction(actionObj, uid, retwits, "retwit");

            } else if (action === "unretwit") {

              minusAction(actionObj, uid, retwits, "retwit");

            }

            resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(likes))});
          } else {
            reject('Unauthorised');
          }

          return;
        }

        //Add new comment
        if (url.endsWith('/comment/new') && opts.method === 'POST') {
          let {comment, uid, commentFor} = JSON.parse(opts.body);
          let newId = comments.length ? Math.max(...comments.map(com => com.id)) + 1 : 1;
          const filteredUser = users.find(user => user.id === uid);
          const filteredCard = cards.find(card => card.id === comment.postId);
          if (commentFor === "comment") {
            const replaysToUser = comments.find(com => com.id === comment.replaysTo ? com.replays = com.replays + 1 : null);
            Object.assign(comment, {
              id: newId,
              userId: filteredUser.id,
              userComName: filteredUser.username,
              replaysName: [ filteredCard.userName, replaysToUser.userComName ],
              replays: 0,
            });
          } else {
            Object.assign(comment, {
              id: newId,
              userId: filteredUser.id,
              userComName: filteredUser.username,
              replaysName: [ filteredCard.userName ],
              replays: 0,
              replaysTo: null
            });
          }

          comments.push(comment);
          cards.filter(card => card.id === comment.postId ? card.comments = card.comments + 1 : card)

          localStorage.setItem('comments', JSON.stringify(comments));
          localStorage.setItem('cards', JSON.stringify(cards));

          let matchedComments = comments.filter(com => com.postId === comment.postId);
          let newComments = matchedComments.length ? matchedComments : [];

          resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(newComments))});

          return;
        }

        //GET comments by id
        if (url.match(/\/comments\/\d+$/) && opts.method === 'GET') {
          let urlParts = url.split('/');
          let id = parseInt(urlParts[urlParts.length - 1]);
          let matchedComments = comments.filter(comment => comment.id === id);
          if (matchedComments) {
            let comment = matchedComments;
            let cardArr = [];

            matchedComments.forEach((elem) => {
              cards.forEach((item) => {
                if(elem.postId === item.id){
                  cardArr.push(item);
                  return;
                }
              });
            });

            comments.forEach((item) => {
              if (item.replaysTo && item.replaysTo === id) {
                comment.push(item);
              }
            });

            resolve({ ok: true, text: () => Promise.resolve(JSON.stringify({
              card: cardArr,
              comment: comment}))});
          } else {
            reject('No comment match your query');
          }

          return;
        }

        //GET numbers for boards
        if (url.endsWith('/boards') && opts.method === 'GET') {
          resolve({ ok: true, text: () => Promise.resolve(JSON.stringify({
            like: likes,
            retwit: retwits
          }))});
        }

        realFetch(url, opts).then(response => resolve(response));

      }, 1);
    });
  }


//imitate someone add new post
  setTimeout(() => {
    let newPost = {
      id: null,
      time: Date.now(),
      picture: "",
      text: "text",
      like: 0,
      retwit: 0,
      comments: 0,
      userId: 2,
      userName: "userName2"
    }

    newPost.id = cards.length ? Math.max(...cards.map(card => card.id)) + 1 : 1;
    cards.push(newPost);
    localStorage.setItem('cards', JSON.stringify(cards));

  }, 3000)
}
