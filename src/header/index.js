import React from 'react';
import { Link } from "react-router-dom";

import { connect } from 'react-redux';

import Avatar from '../components/Avatar'
import Modal from '../portals/Modal'
import { userActions } from '../actions';

import './header.css';

class Header extends React.Component{
  state = {
    isModalOpen: false
  }

  handleProtal = () => {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    })
  }

  handleClick = (name, pass) => {
    if(this.props.loggedIn){
      this.props.dispatch(userActions.logout());
    } else {
      this.props.dispatch(userActions.login(name, pass));
    }
    this.setState({
      isModalOpen: false
    })
  }

  render(){
    const { user, loggedIn } = this.props;
    console.log("HEADER", this.props)
      return (
      <header className="container-fluid">
        <div className="container">
          <div className="row">
          <div className="col-6">
            <Link to="/" className="header-link">Main</Link>
          </div>
          <div className="col-6 d-flex justify-content-end relative" id="header">
            <div className="header-hover" onClick={this.handleProtal}>
              {!user && <div>LOGIN</div>}
              {loggedIn &&
                <>
                  <span> {user.username} </span>
                  <Avatar avatarSize="50" />
                </>}
            </div>
          </div>
            {this.state.isModalOpen &&
               <Modal
                onClick={(name, pass) => this.handleClick(name, pass)}
                porttype={loggedIn ? "logout" : "login"}
                modalRoot="header" />}
          </div>

        </div>
      </header>
    )
  }
}

export default connect(
  state => ({
    user: state.authentication.user,
    loggingIn: state.authentication.loggingIn,
    loggedIn: state.authentication.loggedIn
  })
)(Header)
