export const likes = [
  {
    postId: 3,
    commentId: null,
    userId: [1]
  },
  {
    postId: 1,
    commentId: null,
    userId: [2, 3, 4]
  },
  {
    postId: null,
    commentId: 1,
    userId: [2]
  }
]
