export const comments = [
    {
      id: 0,
      postId: 3,
      userId: 1,
      userComName: "userName1",
      comment: "this is comment from User1",
      replaysName: ["userName2"],
      replays: 0,
      replaysTo: null
    },
    {
      id: 1,
      postId: 3,
      userId: 1,
      userComName: "userName1",
      comment: "this is 2nd comment from User1",
      replaysName: ["userName2"],
      replays: 1,
      replaysTo: null
    },
    {
      id: 2,
      postId: 3,
      userId: 3,
      userComName: "userName3",
      comment: "this is replay to userName1",
      replaysName: ["userName2", "userName1"],
      replays: 0,
      replaysTo: 1
    }
]
